// index.html to be served by the ESP8266 chip

const char MAIN_page[] PROGMEM = R"=====(
<!DOCTYPE html>
<html>
 	<head>
 		<meta name='viewport' content='width=device-width, initial-scale=1.0'/>
 		<meta charset='utf-8'>
		<style>
			body {font-size:100%;} 
			#main {display: table; margin: auto;  padding: 0 10px 0 10px; } 
			h1 {text-align:center; } 
			h2 {text-align:center; } 
			.colourSelect {  
				border: 2px solid black; 
				border-radius: 5px;
				padding: 20px
			}
			.my-button {
				margin: 5px;
				text-align:center;
			}
		</style>
		<title>Virtual MARV - SC3</title>
 		  <script> 
		   	let ajaxRequest = null;
			if (window.XMLHttpRequest){ 
				ajaxRequest = new XMLHttpRequest(); 
			}
			else{ 
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP"); 
			}
			function ajaxLoad(path, isDataRequired, data) {  
				if(!ajaxRequest){ 
					alert('AJAX is not supported.'); 
					return; 
				}

				let route = "";
				if (isDataRequired) {
					route = path + "?" + data
				}
				else {
					route = path;
				}

				ajaxRequest.open('GET', route, true);
				ajaxRequest.onreadystatechange = function(){
					if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
						if (path == "update") {
							console.log("Updated");
							let ajaxResult = ajaxRequest.responseText;
							let array = ajaxResult.split(',');

                            document.getElementById('lineOffset').textContent = array[0];
                            document.getElementById('motorSpeed').textContent = array[1];
                            console.log(array[2]);

                            if (array[2] == 0) {
                                document.getElementById('displayInfo').style.display = "block";
                                document.getElementById('infoDisplay').style.display = "none";
                            }
                            else {
                                document.getElementById('displayInfo').style.display = "none";
                                document.getElementById('infoDisplay').style.display = "block";
                            }
							
						}
						else {
							console.log("Path handled");
						}
					}
				}
				ajaxRequest.send();
			}
			setInterval( function() { ajaxLoad("update", false, 0); }, 1000);
		</script>
 	</head>
 
 	<body>
		<div id='main'>
			<h1>EBB Practical 2</h1>
			<h2>Critical System Diagnostic Information</h2>

            <p id="displayInfo">
                No critical system information available yet.
            </p>

            <div id="infoDisplay" style="display:none;">
                <p id='lineOffset_Display'>
                    Line Offset = <span id='lineOffset'></span> mm
                </p>

                <p id='motorSpeed_Display'>
                    Motor Speed = <span id='motorSpeed'></span> RPM
                </p>
            </div>
		
		</div> 
 	</body>
</html>
)====="; 