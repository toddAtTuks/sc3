/**
 * Author:          T.M. Atterbury
 * Last updated:    21/10/2020
 * 
 * 
 **/

// Libraries
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <string.h>

// Local files
#include "index.h"
#include "server.h"
 

// Web server
String header = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n";
String html = MAIN_page;
WiFiServer server(80);
String request = "";
String tmpString = "";
 
// Network connection
char ssid[] = "HGU-Cloud-Nine";        //  network SSID (name)
char pass[] = "luskeboeties";          //  network password

// Touch sensor
const int touchSensor = D2; 
const int outputLED = D1; 
bool isButtonStateOn = false;

// Non-blocking delay
unsigned long DELAY_TIME = 1000;    // 1 second
unsigned long delayStart = 0;       // the time the delay started
bool delayRunning = false;          // true if still waiting for delay to finish

// Serial communication
char rx_buffer [4];
char tx_bytes [4];
int rx_counter = 0;
bool serialCommsStarted = false;
bool buttonPressed = false;

// Critial System Diagnostic Information 
unsigned int lineOffset = 0; 
unsigned int motorSpeed = 0; 
unsigned int maxMotorSpeed = 0; 

// Interrupt
ICACHE_RAM_ATTR void buttonTouched() {
  // Start the delay if it is not running
  if (!delayRunning) {
      isButtonStateOn = !isButtonStateOn;
      delayStart = millis();
      delayRunning = true;
      digitalWrite(outputLED, HIGH);      // Signal that the button has been touched
  }
}

// Initialization of the Web Server hosting a static webpage displaying the system information
// AJAX requests are use to update the data without refreshing the entire web page
void initializeWebServer(){
  Serial.begin(19200);
//   Serial.println();
//   Serial.println("Serial started at 19200");
//   Serial.println();

  // Connect to a WiFi network
//   Serial.print(F("Connecting to "));  
//   Serial.println(ssid);
  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED) 
  {
    //   Serial.print(".");
      delay(500);
  }

//   Serial.println("");
//   Serial.println(F("CONNECTED"));
//   Serial.print("IP ");              
//   Serial.println(WiFi.localIP()); 

  // Start a server
  server.begin();
//   Serial.println("Server started");
}

// Initialization pin & interrupt used for the touch sensing
void initializeTouchSensor(){
  // Initialize digital pin D1 as an output.
  pinMode(outputLED, OUTPUT);

  // Set analogInPin pin as interrupt, assign interrupt function and set RISING mode
  attachInterrupt(digitalPinToInterrupt(touchSensor), buttonTouched, RISING);
}

void writeBytes(char bytes[]) {
    for(int i=0; i<4; i++) {
        Serial.write(bytes[i]);
    }
}

void processReceivedCommand() {

    // Start condition
    if (!serialCommsStarted) {
        if ((rx_buffer[0] == 0) && 
            (rx_buffer[1] == 0) && 
            (rx_buffer[2] == 0) && 
            (rx_buffer[3] == 0)) {
                serialCommsStarted = true;
                int x = isButtonStateOn;
                char bytes[4] = {16, x, 71, 0};
                writeBytes(bytes);
            }
    }
    
    // Serial comms
    else {
        // Button not pressed yet
        if (rx_buffer[0] == 16) {

                int x = isButtonStateOn;
                char bytes[4] = {16, x, 71, 0};

                // Only echo if the button is not on
                if (rx_buffer[1] == 0)  {
                    writeBytes(bytes); 
                }
        }

        else {   // Receiving offset and speed data

            if (rx_buffer[0] == 163) {
                motorSpeed = rx_buffer[1];
                maxMotorSpeed = motorSpeed + rx_buffer[2];
            }

            else if (rx_buffer[0] == 177) {
                lineOffset = rx_buffer[1];

                // Send success command back
                int halfMaxSpeed= maxMotorSpeed * 0.5;
                char bytes[4] = {145, halfMaxSpeed, halfMaxSpeed, 0};
                writeBytes(bytes);
            }
            else {
                digitalWrite(outputLED, HIGH); 
            }
        }
    }
}

void handleSerialComms() {
    if (Serial.available() >= 4){
        Serial.readBytes(rx_buffer, 4);
        processReceivedCommand();
    }
    else {
        // Still waiting for 4 bytes of data
    }
}

///////////////////////////////////////////////
//                  SETUP                    //
///////////////////////////////////////////////
void setup() 
{
    initializeWebServer();   
    initializeTouchSensor();
} 
 
///////////////////////////////////////////////
//                   LOOP                    //
///////////////////////////////////////////////
void loop() 
{
    // Check if a client has connected
    WiFiClient client = server.available();
    if (!client)  {  return;  }

    // Read the first line of the request
    request = client.readStringUntil('\r');
 
    // Route handling
    if (request.indexOf("update") > 0) {    // When there is a GET request to /udpate, send back the update system information
        client.print( header );
        String payload;
        payload += lineOffset;
        payload += ",";
        payload += motorSpeed;
        payload += ",";
        payload += isButtonStateOn;
        client.print( payload ); 
    }
    else {
        client.flush();
        client.print( header );
        client.print( html );   
    }

    // Serial comms
    handleSerialComms();

    // Touch button
    // Check if delay has timed out, and reset.
    if (delayRunning && ((millis() - delayStart) >= DELAY_TIME)) {
        delayStart += 0;       
        delayRunning = false;
        digitalWrite(outputLED, LOW);  
    }
} 